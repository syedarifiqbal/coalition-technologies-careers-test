@extends('layouts.master')

@section('content')

<div class="container mt-5" id="form">

    <div class="row">

        <div class="col">

            <form class="form-inline" @submit.prevent="save">

                <input type="text" required class="form-control mb-2 mr-sm-2" v-model="form.product" placeholder="Product">

                <input type="number" required class="form-control mb-2 mr-sm-2" v-model="form.quantity" placeholder="Quantity">

                <input type="number" required class="form-control mb-2 mr-sm-2" v-model="form.price" placeholder="Price">

                <button type="submit" class="btn btn-primary mb-2">Save</button>

            </form>

        </div>

    </div>

    <div class="row" v-if="orders.length > 0">
        <div class="col">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Value</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
                <tr v-for="order in orders">
                    <td v-text="order.product"></td>
                    <td v-text="order.quantity"></td>
                    <td v-text="order.price"></td>
                    <td v-text="order.quantity * order.price"></td>
                    <td v-text="order.created_at"></td>
                    <td>
                        <button class="btn btn-default" @click="editHandler(order.created_at)">Edit</button>
                    </td>
                </tr>
                <tr>
                    <th>Total</th>
                    <th></th>
                    <th>@{{ totalPrice }}</th>
                    <th>@{{ totalAmount }}</th>
                    <th></th>
                    <th></th>
                </tr>
            </table>
        </div>
    </div>

</div>

@endsection

@section('footer_scripts')
    <script>
        new Vue({
            el: '#form',
            data: {
                edit: false,
                form: {
                    product: '',
                    quantity: '',
                    price: ''
                },
                orders: [],
                currentOrder: {}
            },
            methods: {
                save(){
                    if(this.edit)
                    {
                        this.editRecord();
                    }
                    else{
                        this.insertRecord();
                    }
                },
                insertRecord(){
                    axios.post('/orders', this.form)
                        .then(data => {
                            this.orders.push(data.data);
                            this.resetFrom();
                        }).catch((error) => {
                            alert('please fill the form with valid data');
                        });
                },
                resetFrom(){
                    this.form.product = '';
                    this.form.quantity = '';
                    this.form.price = '';
                },

                editHandler(timestamp){
                    this.edit = true;
                    this.currentOrder = this.findOrderByTime(timestamp);
                    this.form.product = this.currentOrder.product;
                    this.form.quantity = this.currentOrder.quantity;
                    this.form.price = this.currentOrder.price;
                },

                editRecord(){
                    this.edit = true;
                    axios.put('/orders/'+this.currentOrder.created_at, this.form)
                        .then(data => {
                            this.orders.forEach((o)=>{
                                if(o.created_at === this.currentOrder.created_at)
                                {
                                    o.product = this.form.product;
                                    o.quantity = this.form.quantity;
                                    o.price = this.form.price;
                                }
                            });
                            this.edit = false;
                            this.resetFrom();
                        });
                },

                findOrderByTime(timestamp){
                    let order = this.orders.filter(function(o){
                        return o.created_at === timestamp;
                    });
                    return order.length>0? order[0] : false;
                },
                loadData(){
                    axios.get('/orders')
                        .then(data => {
                            this.orders = data.data;
                        })
                }
            },
            mounted(){
                this.loadData();
            },
            computed: {
                totalPrice(){
                    let price = 0;
                    this.orders.forEach(function(o){
                        price += parseFloat(o.price);
                    });
                    return price;
                },
                totalAmount(){
                    let amount = 0;
                    this.orders.forEach(function(o){
                        amount += parseFloat(o.price) * parseFloat(o.quantity);
                    });
                    return amount;
                }
            }
        })
    </script>
@endsection