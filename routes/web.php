<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/orders', 'OrdersController@index');
Route::post('/orders', 'OrdersController@store');
Route::put('/orders/{timestamp}', 'OrdersController@update');
