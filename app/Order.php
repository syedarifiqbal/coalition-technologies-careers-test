<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Order extends Model
{
    public static function getOrders()
    {
        $content = Storage::disk('local')->get('orders.json');
        $content = json_decode($content);
        if (!$content)
            $content = [];
        return $content;
    }

    public static function findOrder($timestamp)
    {
        $orders = self::getOrders();
        foreach($orders as $order)
        {
            if($order->created_at === $timestamp)
                return $order;
        }
        return false;
    }
}
