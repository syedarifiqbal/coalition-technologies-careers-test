<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Order::getOrders();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|string
     */
    public function store(Request $request)
    {
        $request->validate([
            'product' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        $content = Order::getOrders();
        $data = [
            'product' => $request->input('product'),
            'quantity' => $request->input('quantity'),
            'price' => $request->input('price'),
            'created_at' => now()->format('Y-m-d H:i:s'),
        ];
        $content[] = $data;

        Storage::disk('local')->put('orders.json', json_encode($content));

        return $data;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $timestamp)
    {
        $request->validate([
            'product' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        $order = Order::findOrder($timestamp);
        $data = Order::getOrders();
        $requestData = [
            'product' => $request->input('product'),
            'quantity' => $request->input('quantity'),
            'price' => $request->input('price'),
        ];
        foreach($data as $key => $row)
        {
            if($row->created_at === $order->created_at)
            {
//                dd($requestData);
                $requestData['created_at'] = $order->created_at;
                $data[$key] = $requestData;
            }
        }
        Storage::disk('local')->put('orders.json', json_encode($data));

        return $requestData;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
